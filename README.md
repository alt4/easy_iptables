# Ansible role: easy_iptables

Made for **Northamp**.

Based on an [article on unrouted.io](https://unrouted.io/2017/08/15/docker-firewall/).

## Requirements

Ansible 2.x on a Debian-based system.

Tested with Ansible 2.9 on Debian 10.

## Description

This role creates a service that applies iptables with as less disruption on other running services as possible.

It creates two new chains (`IN-FILTERS` and `OUT-FILTERS`) that are appended to the default `INPUT` and `OUTPUT` chains.

The latter two are set to `DROP` every communications that aren't in the new chains, except inbound ICMP and established communications.

The custom chains are populated using variables containing list of iptables rules formatted like an `iptables-save` output.

Supported services are:
* `Docker` (**untouched** 👌)
* `Fail2ban` (reloaded after applying rules)

## Variables

```yaml
easy_iptables_in_rules:
  - "-j ACCEPT"
```
List containing the iptables rules allowing inbound trafic. *DO NOT* specify chains (-A).

```yaml
easy_iptables_out_rules:
  - "-j ACCEPT"
```
List containing the iptables rules allowing outbound trafic. *DO NOT* specify chains (-A).

## Dependencies

None

## Example Playbook

This playbook allows inbound SSH, HTTP and HTTPS as well as outbound DNS on their default ports, and everything else is blocked.

```yaml
- hosts: all
  vars:
    - easy_iptables_in_rules:
      - "-m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT"
      - "-m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT"
      - "-m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT"
    - easy_iptables_out_rules:
      - "-m state --state NEW -m tcp -p tcp --dport 53 -j ACCEPT"
      - "-m state --state NEW -m tcp -p udp --dport 53 -j ACCEPT"
  roles:
    - easy_iptables
```

## License

MIT